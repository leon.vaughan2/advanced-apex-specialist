/**=====================================================================
* Appirio, Inc
* Name: Constants
* Description: Constants class for static / constant values
* Created Date: 30 June 2021
* Created By: Leon Vaughan(Appirio)
=======================================================================*/
public class Constants {
    public static final INTEGER DEFAULT_ROWS = 5;
    
    public static final STRING SELECT_ONE = System.Label.Select_One;
    
    public static final STRING INVENTORY_LEVEL_LOW = System.Label.Inventory_Level_Low;
    
    public static final List<Schema.PicklistEntry> PRODUCT_FAMILY = getProductFamily();
        
    public static final STRING DRAFT_ORDER_STATUS = 'Draft';
    
    public static final STRING ACTIVATED_ORDER_STATUS = 'Activated';
    
    public static final STRING INVENTORY_ANNOUNCEMENTS = 'Inventory Announcements';
    
    public static final STRING ERROR_MESSAGE = 'An error has occurred, please take a screenshot with the URL and send it to IT.';
    
    public static final ID STANDARD_PRICEBOOK_ID = '01s4L000002r4PAQAY';
    
    public static List<Schema.PicklistEntry> getProductFamily() {
        Schema.DescribeFieldResult productFamilyDescribeFieldResult = Product2.Family.getDescribe();
        List<Schema.PicklistEntry> picklistValues = productFamilyDescribeFieldResult.getPickListValues();
        return picklistValues;
    }

}